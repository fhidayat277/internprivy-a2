// single element
//console.log(document.getElementById('my-form'));
//console.log(document.querySelector('h1'));

//multiple element
//console.log(document.querySelectorAll('.item'));
//console.log(document.getElementsByClassName('item'));
//console.log(document.getElementsByTagName('li'));

/*const ul = document.querySelector('.items');
//ul.remove();
//ul.lastElementChild.remove();
ul.firstElementChild.textContent = 'yuhuuu';
ul.children[1].innerText = 'Yahooo';
ul.lastElementChild.innerHTML = '<h3>Wuhuuuu</h3>';

const btn = document.querySelector('.btn');
btn.style.background = 'black';*/

/*const btn = document.querySelector('.btn');
//mouseover, mouseout
btn.addEventListener('click', (e) => {
    e.preventDefault();
    document.querySelector('#my-form')
    .style.background = '#acc';
    document.querySelector('body').classList.add('bg-dark');
    document.querySelector('.items').lastElementChild
    .innerHTML = '<h1>Hallo</h1>';
});*/

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onsubmit);

function onsubmit(e) {
    e.preventDefault();

    if (nameInput.value === '' || emailInput.value === ''){
        //alert('Silahka isi Kolom');
        msg.classList.add('error');
        msg.innerHTML= 'Silahkan isi semua kolom'

        setTimeout(() => msg.remove(), 3000);
    } else {
        //console.log('berhasil');
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(`${
            nameInput.value} : ${emailInput.value}`));
    
        userList.appendChild(li);

        // hapus kolom
        nameInput.value = '';
        emailInput.value = '';
    }
}
//console.log('Helloooooww');
//console.error('ini adalah error');
//console.warn('ini adalah warning');

// let, const
// perbedaan nyaadalah you can reassign values
// untuk penggunaan const harus di inialisasi pada fungsinya langsung
//let score;
//score = 10;
//const score=10;
//console.log(score);

//string, boolean, null,  undefinied, symbol, 
//const name = 'Pipit';
//const age = 25;
//const rating = 7.3;
//const IsCool = true;
//const x = null;
//const y = undefined;
//let z;
//console.log(typeof y);

//concatenation
//const name = 'Pipit';
//const age = 21;
//console.log('Namaku ' + name + ' dan Umurku ' + age );
//Tempaplte String
//console.log(`Namaku ${name} dan umurku ${age}`);
//const hallo = `Namaku ${name} dan umurku ${age}`;
//console.log(hallo);

//const s = 'Hallo Pipit';
//console.log(s.length);
//console.log(s.toUpperCase());
//console.log(s.substring(0, 5).toUpperCase());
//console.log(s.split(''));
//const s = 'Bandung, Jogja, Bali';
//console.log(s.split(', '));

//Arrays - variables that hold multiple values
/*Multi
Line
Comment*/
//const numbers = new Array(1,2,3,4,5,6,7);
//console.log (numbers);
/*const Buah = ['apel', 'mangga', 'durian', 10, true];
Buah[3] = 'anggur';
Buah.push('Pir');
Buah.unshift('jangung');
Buah.pop();
console.log(Array.isArray(Buah));
//console.log(Array.isArray('hallo'));
console.log(Buah.indexOf('durian'));
console.log (Buah);*/

/*const person = {
    Namadepan : 'Pipit',
    Namabelakang: 'Sapitra',
    umur: 20,
    hobi : ['Futsal', 'game', 'nongkrong'],
    alamat : {
    jalan : 'Jalan Tinalan Nno 598',
    kota : 'Jogja',
    status : 'Single'
    }
}*/
//console.log(person);
/*console.log(person.Namabelakang, person.Namadepan);
console.log(person.hobi[2]);
console.log(person.alamat.kota);*/
/*const {Namadepan, Namabelakang, alamat: {kota}} = person;
person.email = 'pipt@gmail.com';
console.log(person);*/
//console.log(kota);

/*const todos = [
    {
        id: 1,
        text: 'Buang sampah',
        isCompleted : true
    },
    {
        id : 2,
        text : 'Cuci Baju',
        isCompleted : true
    },
    {
        id : 3,
        text : 'Ke Bank',
        isCompleted : true
    },
    {
        id : 4,
        text : 'Cari Makan',
        isCompleted : false
    }

];*/
//console.log(todos[1].text);
//const todoJson = JSON.stringify(todos);
//console.log(todoJson);

/* for 
for (let i = 0; i <= 10; i++){
    console.log(`For loop number : ${i}`);
}

//while loop
let i = 0;
while(i < 10) {
    console.log(`While loop number : ${i}`);
    i++;
}*/
/*for (let i = 0; i < todos.length; i++){
    console.log(todos[i].text);
}*/
/*for(let todo of todos){
    console.log(todo.text);

}*/

// foreach, map, filter
/*todos.forEach(function(todo){
    console.log(todo.text);
});
/*const todoText = todos.map(function(todo){
    return todo.text;
});*/
/*const todoCompleted = todos.filter(function(todo){
    return todo.isCompleted === true;
}).map(function(todo){
    return todo.text;
})
console.log(todoCompleted);*/

// conditional
//const x = 6;
//const y = 18;
// equal dobell atau triple sama saja kecuali variable x diganti ke string ' '
/*if (x > 5 ) {
    console.log('x adalah 10');
} else if (x  > 10) {
    console.log('x lebih besar dari 10');
}else{
    console.log('x kurang dari 10');
}*/
//if (x > 5 || y > 10 ) {
  //  console.log('x lebih dari 5 atau y lebih dari 10');}

//switch
/*const x = 1;
const color = 'green';
//const color = x > 10 ? 'merah' : 'biru';

switch(color){
    case 'merah':
        console.log('Warna merah');
        break;
    case 'biru':
        console.log('warna biru');
        break;
    default:
        console.log('warna bukan merah atau biru');
        break;
}*/

//function
/*function addNums(num1, num2){
    console.log(num1 + num2);
}
addNums(7,6);*/

//construktor Function
/*function Person(firstName, lastName, DoB) {
    // Set object properties
    this.firstName = firstName;
    this.lastName = lastName;
    this.DoB = DoB;
}
//initiate object
const person1 = new Person('Pipit', 'sapitra', '1-1-1991');
const person2 = new Person('Maman', 'Sarman', '2-2-1992');

console.log(person2.DoB);*/

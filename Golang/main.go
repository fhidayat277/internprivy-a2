package main

import (
	"boking-app/helper"
	"fmt"
	"sync"
	"time"
)

const conferenceTickets int = 50

var conferenceName = "Go Conference"
var remainingTickets int = 50
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets int
}

var wg = sync.WaitGroup{}

func main() {

	greetUsers()

	//fmt.Printf("conferenceTickets is %T, remainingTickets is %T, conference is %T\n", conferenceTickets, remainingTickets, conferenceName)

	//ln untukotomatis line baru
	//fmt.Printf("Welcome to %v booking application\n", conferenceName)
	//fmt.Println(conferenceName)

	firstName, lastName, email, userTickets := getUserInput()
	isValidName, isValidEmail, isValidTicketNumber := helper.ValidateUserInput(firstName, lastName, email, userTickets, remainingTickets)

	//if userTickets > remainingTickets
	if isValidName && isValidEmail && isValidTicketNumber {

		bookTicket(userTickets, firstName, lastName, email)
		wg.Add(1)
		go sendTicket(userTickets, firstName, lastName, email)
		//call function print first name
		firstNames := getFirstNames()
		fmt.Printf("the first name of bookings are : %v\n", firstNames)

		// dobel == ( untuk perbandingan 2 kondisi) = (untuk menjelaskan to variable )

		if remainingTickets == 0 {
			// program akan berkahir keitka tiket 0
			fmt.Println("our conference is booked out. come back next year.")
			//	break
		}
	} else {
		if !isValidName {
			fmt.Println("first name or last name you entered is too short")
		}
		if !isValidEmail {
			fmt.Println("email address you entered doesn't contain @ sign")
		}
		if !isValidTicketNumber {
			fmt.Println("number of tickets you entered is invalid")
		}
	}
	wg.Wait()
}

func greetUsers() {
	fmt.Printf("Welcome to %v booking application \n", conferenceName)
	fmt.Printf("we have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
	fmt.Println("get your ticket here to attend")
}
func getFirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}
	//fmt.Printf("these are all our bookings : %v\n", bookings)
	//fmt.Printf("the first names of bookings are : %v\n", firstNames)
	return firstNames
}

func getUserInput() (string, string, string, int) {
	var firstName string
	var lastName string
	var email string
	var userTickets int

	// ask to user for  the name
	fmt.Println("enter your first name: ")
	fmt.Scan(&firstName)

	fmt.Println("enter your lastname: ")
	fmt.Scan(&lastName)

	fmt.Println("enter your email: ")
	fmt.Scan(&email)

	fmt.Println("No Tickets : ")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets
}

func bookTicket(userTickets int, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	// var myslice []string
	// var mymap map [string]string
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	//bookings[0] = firstName + " " + lastName
	bookings = append(bookings, userData)
	fmt.Printf("List of Booking is %v \n", bookings)

	fmt.Printf("Thankyou %v %v for booking %v tickets. you will receive a conifrmation email at %v\n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)
}

func sendTicket(userTickets int, firstName string, lastName string, email string) {
	time.Sleep(50 * time.Second)
	var ticket = fmt.Sprintf("%v tickets for %v %v ", userTickets, firstName, lastName)
	fmt.Println("##########################")
	fmt.Printf(" sending ticket:\n %v \nto email address %v \n", ticket, email)
	fmt.Println("##########################")
	wg.Done()
}
